module.exports = {
  siteMetadata: {
    title: 'Gatsby Starter Blog using DatoCMS, with Bulma Sass',
    author: 'Travis Reynolds',
    description: 'A starter blog with Bulma Sass, which uses DatoCMS as a backend.',
    siteUrl: 'https://gitlab.com/cirruscreative/templates/gatsby-blog-bulma',
  },
  pathPrefix: '/gatsby-blog-datocms-bulma',
  plugins: [
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `${__dirname}/src/pages`,
        name: 'pages',
      },
    },
    {
      resolve: `gatsby-source-datocms`,
      options: {
        apiToken: `YOUR_TOKEN`,
      },
    },
    {
      resolve: `gatsby-transformer-remark`,
      options: {
        plugins: [
          {
            resolve: `gatsby-remark-images`,
            options: {
              maxWidth: 590,
            },
          },
          {
            resolve: `gatsby-remark-responsive-iframe`,
            options: {
              wrapperStyle: `margin-bottom: 1.0725rem`,
            },
          },
          'gatsby-remark-prismjs',
          'gatsby-remark-copy-linked-files',
          'gatsby-remark-smartypants',
        ],
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        //trackingId: `ADD YOUR TRACKING ID HERE`,
      },
    },
    `gatsby-plugin-feed`,
    `gatsby-plugin-offline`,
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-sass`
  ],
}
