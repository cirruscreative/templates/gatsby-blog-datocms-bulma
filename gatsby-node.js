const path = require(`path`)
const { createFilePath } = require(`gatsby-source-filesystem`)

exports.createPages = ({ graphql, boundActionCreators }) => {
  const { createPage } = boundActionCreators

  return new Promise((resolve, reject) => {
    graphql(`
      {
        allDatoCmsBlogPost {
          edges {
            node {
              slug
            }
          }
        }
      }
    `).then(result => {
      result.data.allDatoCmsBlogPost.edges.map(({ node: post }) => {
        createPage({
          path: `/posts/${post.slug}`,
          component: path.resolve(`./src/templates/blog-post.js`),
          context: {
            slug: post.slug,
          },
        })
      })
      resolve()
    })
  })
}