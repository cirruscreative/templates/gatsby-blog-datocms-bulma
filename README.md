# Gatsby Blog with DatoCMS Backend & Bulma Starter

Gatsby starter for creating a blog with bulma, which uses DatoCMS as a backend

> [DatoCms](https://www.datocms.com/)

Install this starter by cloning the repo, then installing dependancies:
```powershell
git clone https://gitlab.com/cirruscreative/templates/gatsby-blog-datocms-bulma.git your-folder
cd your-folder
yarn (or npm i)
```

## Add Your DatoCMS Token

Add your DatoCMS Readonly token in `gatsby-config.js`:

```powershell
{
  resolve: `gatsby-source-datocms`,
  options: {
    apiToken: `10ad86a19770054cabcc9e78743bd5`,
  },
}
```

## Running in development

`gatsby develop`

## Build

`gatsby build`

### Building for Gitlab Pages

Edit the path prefix in `gatsby-config.js`:

```powershell
pathPrefix: '/your-repo-path'
```

Then build:

`gatsby build --prefix-paths`