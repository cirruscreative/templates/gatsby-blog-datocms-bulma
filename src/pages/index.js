import React from 'react'
import Link from 'gatsby-link'
import Img from 'gatsby-image'

const IndexPage = ({ data }) => (
  <div>
    {data.allDatoCmsBlogPost.edges.map(({ node: post }) => (
      <Link to={`/posts/${post.slug}`} className="card" key={post.id}>
        <div className="card-content">
          <p className="title">
          {post.title}
          </p>
          <p className="subtitle">
            {post.text}
          </p>
        </div>
      </Link>
    ))}
  </div>
)

export default IndexPage

export const query = graphql`
  query IndexQuery {
    allDatoCmsBlogPost(sort: { fields: [position], order: ASC }) {
      edges {
        node {
          id
          title
          slug
          text
          featuredImage {
            sizes(maxWidth: 450, imgixParams: { fm: "jpg", auto: "compress" }) {
              ...GatsbyDatoCmsSizes
            }
          }
        }
      }
    }
  }
`