import React from 'react'
import Link from 'gatsby-link'
import { HelmetDatoCms } from 'gatsby-source-datocms'
import Img from 'gatsby-image'

const About = ({ data: { about } }) => (
  <div className="container">
    <h1 className="title"
    dangerouslySetInnerHTML={{
      __html: about.aboutTitle
    }}>
    </h1>
    <div
      dangerouslySetInnerHTML={{
        __html: about.aboutTextNode.aboutText
      }}
    />
  </div>
)

export default About

export const query = graphql`
  query AboutQuery {
    about: datoCmsHome {
      aboutTitle
      aboutTextNode {
        id
        aboutText
      }
    }
  }
`