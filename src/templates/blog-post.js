import React from 'react'
import { HelmetDatoCms } from 'gatsby-source-datocms'
import Img from 'gatsby-image'

export default ({ data }) => (
  <div className="container">
  <HelmetDatoCms seo={data.datoCmsBlogPost.seoMetaTags} />
    <h1 className="title">
    {data.datoCmsBlogPost.title}
    </h1>
    <div
      dangerouslySetInnerHTML={{
        __html: data.datoCmsBlogPost.textNode.childMarkdownRemark.html,
      }}
    />
  </div>
)

export const query = graphql`
  query PostsQuery($slug: String!) {
    datoCmsBlogPost(slug: { eq: $slug }) {
      seoMetaTags {
        ...GatsbyDatoCmsSeoMetaTags
      }
      title
      textNode {
        childMarkdownRemark {
          html
        }
      }
      featuredImage {
        url
        sizes(maxWidth: 600, imgixParams: { fm: "jpg", auto: "compress" }) {
          ...GatsbyDatoCmsSizes
        }
      }
    }
  }
`