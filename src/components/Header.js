import React from 'react'
import Link from 'gatsby-link'
import Img from 'gatsby-image'

const Header = ({data}) => (
  <nav className="navbar is-transparent">
    <div className="navbar-brand">
      <Link className="navbar-item" to="/">
        <img src="https://bulma.io/images/bulma-logo.png" alt="Bulma: a modern CSS framework based on Flexbox" width="112" height="28" />
      </Link>
      <div className="navbar-burger burger">
        <span></span>
        <span></span>
        <span></span>
      </div>
    </div>

    <div className="navbar-menu">
      <div className="navbar-start">
        <Link className="navbar-item" to="/about">
          About
        </Link>
      </div>

      <div className="navbar-end">
      </div>
    </div>
  </nav>
)

export default Header
