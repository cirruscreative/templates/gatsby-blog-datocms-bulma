import React from 'react'
import PropTypes from 'prop-types'
import Link from 'gatsby-link'
import { HelmetDatoCms } from 'gatsby-source-datocms'

// Import Components
import Header from '../components/Header'

// Import Bulma & Custome Sass
import '../assets/sass/main.sass'

const Template = ({ children, data}) => (
  <div>
    <HelmetDatoCms
      favicon={data.datoCmsSite.faviconMetaTags}
      seo={data.datoCmsHome.seoMetaTags}
    />
    <Header />
    <div className="section">
      {children()}
    </div>
  </div>
)

Template.propTypes = {
  children: PropTypes.func,
}

export default Template

export const query = graphql`
  query SiteQuery {
    datoCmsSite {
      globalSeo {
        siteName
      }
      faviconMetaTags {
        ...GatsbyDatoCmsFaviconMetaTags
      }
    }
    datoCmsHome {
      seoMetaTags {
        ...GatsbyDatoCmsSeoMetaTags
      }
    }
  }
`
